FROM nginx:alpine

ENV SPAWNER_HOST="container-spawner"
ENV CONTAINER_PREFIX="vscode"
ENV CONTAINER_PORT="8080"

RUN rm -rf /etc/nginx/conf.d/default.conf
COPY director.conf /etc/nginx/templates/director.conf.template
