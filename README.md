# Container Director

[![Build Status](https://drone.jacknet.io/api/badges/jackhadrill/container-director/status.svg)](https://drone.jacknet.io/jackhadrill/container-director)

A tool to proxy an HTTP backend, and failover to a "container spawner" if the backend does not exist.

## Usage

This container listens on port 8080, and relies on [**OAuth2 Proxy**](https://github.com/oauth2-proxy/oauth2-proxy) for authentication.

Environment variables:
| Variable name | Example value | Description |
| ------------- | ------------- | ----------- |
| SPAWNER_HOST | `container-spawner` | The hostname of the [**Container Spawner**](https://git.jacknet.io/jackhadrill/container-spawner), responsible for spawning new containers. |
| CONTAINER_HOST_PREFIX | `vscode` | A prefix used by the [**Container Spawner**](https://git.jacknet.io/jackhadrill/container-spawner) which, when prepended to the user's username, represents the user's bespoke container hostname.<br />Example: `vscode-jack` |
| CONTAINER_PORT | `8080` | The port number used by the spawned container for incoming connections. |

An example `docker-compose.yml` is shown below.

```yml
version: '3'
services:
  auth:
    image: bitnami/oauth2-proxy:latest
    restart: always
    networks:
      - proxy
      - backend
    command:
      - '--upstream=http://container-director:8080/'
      - '--http-address=0.0.0.0:4180'
      - '--skip-provider-button=true'
      - '--whitelist-domain=...'
    environment:
      OAUTH2_PROXY_COOKIE_SECRET: '...'
      OAUTH2_PROXY_INSECURE_OIDC_ALLOW_UNVERIFIED_EMAIL: 'true'
      OAUTH2_PROXY_EMAIL_DOMAINS: '*'
      OAUTH2_PROXY_PROVIDER: 'oidc'
      OAUTH2_PROXY_OIDC_ISSUER_URL: '...'
      OAUTH2_PROXY_CLIENT_ID: '...'
      OAUTH2_PROXY_CLIENT_SECRET: '...'
  container-director:
    image: git.jacknet.io/jackhadrill/container-director:latest
    restart: always
    environment:
      SPAWNER_HOST: "container-spawner"
      CONTAINER_HOST_PREFIX: "vscode"
      CONTAINER_PORT: 8080
  container-spawner:
    image: git.jacknet.io/jackhadrill/container-spawner:latest
    restart: always
    environment:
      CONTAINER_IMAGE: "git.jacknet.io/jackhadrill/code-server:latest"
      CONTAINER_PREFIX: "vscode"
      CONTAINER_NETWORK: "vscode_backend"
      CONTAINER_PERSIST: "/home/coder"
networks:
  backend:
  proxy:
    name: web_proxy
    external: True
```

## Process description

The following steps describe how [**Container Spawner**](https://git.jacknet.io/jackhadrill/container-spawner) and **Container Director** work together in order to spawn bespoke containers, on behalf of users authenticated through **OAuth2 Proxy**.

1. A user logs in via **OAuth2 Proxy**.
   - After a successful login **OAuth2 Proxy** proxies the original request to **Container Director**, adding a `X-Forwarded-Preferred-Username` request header, which contains the user's username.
2. **Container Director** tries to proxy the original request once more to the user's bespoke container `http://<CONTAINER_HOST_PREFIX>-<X-Forwarded-Preferred-Username>:<CONTAINER_PORT>`.
   - For example: `http://vscode-jack:8080`.
3. If the bespoke container does not respond, **Container Director** will proxy the same request to [**Container Spawner**](https://git.jacknet.io/jackhadrill/container-spawner).
4. [**Container Spawner**](https://git.jacknet.io/jackhadrill/container-spawner) returns a loading page with a refresh timer. Meanwhile, it will check to see if the user's bespoke container exists.
   - If not, the user's bespoke container will be created.
   - If it does, something has gone wrong. Return a useful error message!


